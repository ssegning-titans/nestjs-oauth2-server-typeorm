import { IAccessToken } from '@ssegning-nestjs/oauth2-server-core';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ClientEntity } from './client.entity';
import { JoinColumn } from 'typeorm/browser';
import { UserEntity } from './user.entity';

@Entity('oauth_access_tokens')
export class AccessTokenEntity implements IAccessToken {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: 'text' })
  accessToken: string;

  @Column()
  accessTokenExpiresAt: Date;

  @ManyToOne(() => ClientEntity, { nullable: false })
  @JoinColumn({ name: 'client_id' })
  client: ClientEntity;

  @Column({ type: 'text' })
  scope: string;

  @ManyToOne(() => UserEntity, { nullable: false })
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
