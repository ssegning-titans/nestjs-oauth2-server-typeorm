import { IAccessToken, IRefreshToken } from '@ssegning-nestjs/oauth2-server-core';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { ClientEntity } from './client.entity';
import { JoinColumn } from 'typeorm/browser';
import { UserEntity } from './user.entity';

@Entity('oauth_refresh_tokens')
export class RefreshTokenEntity implements IRefreshToken {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ type: 'text' })
  refreshToken: string;

  @Column()
  refreshTokenExpiresAt: Date;

  @ManyToOne(() => ClientEntity, { nullable: false })
  @JoinColumn({ name: 'client_id' })
  client: ClientEntity;

  @Column({ type: 'text' })
  scope: string;

  @ManyToOne(() => UserEntity, { nullable: false })
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
