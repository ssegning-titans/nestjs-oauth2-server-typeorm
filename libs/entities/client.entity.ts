import { IClient } from '@ssegning-nestjs/oauth2-server-core';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('oauth_clients')
export class ClientEntity implements IClient {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({ nullable: false })
  clientId: string;

  @Column({ nullable: false })
  clientSecret: string;

  @Column({ nullable: false })
  accessTokenLifetime: number;

  @Column('simple-array', { nullable: false })
  grants: string[];

  @Column('simple-array', { nullable: false })
  redirectUris: string[];

  @Column({ nullable: false })
  refreshTokenLifetime: number;
}
