import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { IUser } from '@ssegning-nestjs/oauth2-server-core';

@Entity('oauth_users')
export class UserEntity implements IUser {

  @PrimaryGeneratedColumn()
  id: string;

  @Column({ nullable: true })
  firstName: string;

  @Column({ nullable: true })
  lastName: string;
}
