import { IAuthorizationCode } from '@ssegning-nestjs/oauth2-server-core';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { UserEntity } from './user.entity';
import { JoinColumn } from 'typeorm/browser';
import { ClientEntity } from './client.entity';

@Entity('oauth_authorization_codes')
export class AuthorizationCodeEntity implements IAuthorizationCode {
  @PrimaryGeneratedColumn()
  id: string;

  @ManyToOne(() => ClientEntity, { nullable: false })
  @JoinColumn({ name: 'client_id' })
  client: ClientEntity;

  @Column({ nullable: false })
  code: string;

  @Column()
  expiresAt: Date;

  @Column()
  redirectUri: string;

  @Column()
  scope: string;

  @ManyToOne(() => UserEntity, { nullable: false })
  @JoinColumn({ name: 'user_id' })
  user: UserEntity;
}
