import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthorizationCodeEntity } from '../entities';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthorizationCodeService {
  constructor(
    @InjectRepository(AuthorizationCodeEntity)
    private readonly repository: Repository<AuthorizationCodeEntity>,
  ) {
  }
}
