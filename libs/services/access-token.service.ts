import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { AccessTokenEntity, UserEntity } from '../entities';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AccessTokenService {
  constructor(
    @InjectRepository(AccessTokenEntity)
    private readonly repository: Repository<AccessTokenEntity>,
  ) {
  }
}
