import { ICode, Oauth2ServerInterface } from '@ssegning-nestjs/oauth2-server-core';
import { Injectable } from '@nestjs/common';
import { AccessTokenEntity, AuthorizationCodeEntity, ClientEntity, RefreshTokenEntity, UserEntity } from '../entities';

@Injectable()
export class Oauth2TypeormService
  implements Oauth2ServerInterface<ICode, ClientEntity, UserEntity, AuthorizationCodeEntity, AccessTokenEntity, RefreshTokenEntity> {

  generateAccessToken(client: any, user: any, scope?: string): Promise<string> {
    return Promise.resolve('');
  }

  generateAuthorizationCode(client: any, user: any, scope?: string[]): Promise<string> {
    return Promise.resolve('');
  }

  generateRefreshToken(client: any, user: any, scope?: string): Promise<string> {
    return Promise.resolve('');
  }

  getAccessToken(accessToken: string): Promise<any> {
    return Promise.resolve(undefined);
  }

  getAuthorizationCode(authorizationCode: string): Promise<any> {
    return Promise.resolve(undefined);
  }

  getClient(clientId: string, clientSecret: string): Promise<any> {
    return Promise.resolve(undefined);
  }

  getRefreshToken(refreshToken: string): Promise<any> {
    return Promise.resolve(undefined);
  }

  getUser(username: string, password: string): Promise<any> {
    return Promise.resolve(undefined);
  }

  getUserFromClient(client: any): Promise<any> {
    return Promise.resolve(undefined);
  }

  revokeAuthorizationCode(code: any): Promise<boolean> {
    return Promise.resolve(false);
  }

  revokeToken(token: any): Promise<boolean> {
    return Promise.resolve(false);
  }

  saveAuthorizationCode(code: any, client: any, user: any): Promise<any> {
    return Promise.resolve(undefined);
  }

  saveToken(token: any, client: any, user: any): Promise<any> {
    return Promise.resolve(undefined);
  }

  validateScope(user: any, client: any, scope: string): Promise<string | false> {
    return Promise.resolve(undefined);
  }

  verifyScope(accessToken: any, scope: string): Promise<boolean> {
    return Promise.resolve(false);
  }

}
