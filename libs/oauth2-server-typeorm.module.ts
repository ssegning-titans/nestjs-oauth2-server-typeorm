import { DynamicModule, Module, Provider } from '@nestjs/common';
import { Oauth2ServerCoreModule } from '@ssegning-nestjs/oauth2-server-core';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  AccessTokenService,
  AuthorizationCodeService,
  ClientService,
  Oauth2TypeormService,
  RefreshTokenService,
  UserService,
} from './services';
import { AccessTokenEntity, AuthorizationCodeEntity, ClientEntity, RefreshTokenEntity, UserEntity } from './entities';

const providers: Provider[] = [
  AccessTokenService,
  AuthorizationCodeService,
  ClientService,
  Oauth2TypeormService,
  RefreshTokenService,
  UserService,
];

@Module({})
export class Oauth2ServerTypeOrmModule {

  public static forRoot(): DynamicModule {
    return {
      module: Oauth2ServerTypeOrmModule,
      exports: [
        ...providers,
      ],
      providers: [
        ...providers,
      ],
      global: true,
      imports: [
        TypeOrmModule.forFeature([
          AccessTokenEntity,
          AuthorizationCodeEntity,
          ClientEntity,
          RefreshTokenEntity,
          UserEntity,
        ]),
        Oauth2ServerCoreModule.forRoot({
          useFactory: async (service: Oauth2TypeormService) => ({
            model: service,
          }),
          inject: [Oauth2TypeormService],
        }),
      ],
    };
  }

}
